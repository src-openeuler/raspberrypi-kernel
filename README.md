# raspberrypi-kernel

#### 介绍

Raspberry Pi 内核映像文件。

`kernel.tar.gz` 来自 <https://gitee.com/openeuler/raspberrypi-kernel> 仓库，将 SSH 公钥添加到 gitee.com 的账户中后，可通过执行 `. SOURCE` 来获取 `kernel.tar.gz`。也可以采用本仓库通过 Git LFS 上传的 `kernel.tar.gz`。

#### 软件架构

AArch64

#### 安装教程

`dnf install raspberrypi-kernel`

#### 使用说明

安装 raspberrypi-kernel 后，

1. 内核版本号后缀增加 `.raspi` 标识，形如 `5.10.0-4.0.0.1.raspi.aarch64`。
2. 内核模块安装在 `/lib/modules/` 目录下，对应文件夹名称同内核版本号，如 `5.10.0-4.0.0.1.raspi.aarch64`。
3. 内核映像文件在 `/boot/` 目录下，映像文件名形如 `vmlinuz-5.10.0-4.0.0.1.raspi.aarch64`。同时，该文件会替换原来的 `/boot/kernel8.img` 文件。
4. 设备树文件在 `/boot/` 目录下的文件夹下，该文件夹名称形如 `dtb-5.10.0-4.0.0.1.raspi.aarch64`。同时，该文件夹下的 `.dtb` 文件和 `overlays` 文件夹会替换 `/boot/` 下对应的文件。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
