# raspberrypi-kernel

#### Description

Kernel Image of Raspberry Pi.

The `kernel.tar.gz` is from <https://gitee.com/openeuler/raspberrypi-kernel>. After you put your public ssh key into your account in gitee, you can obtain `kernel.tar.gz` by executing command `. SOURCE`. You can directly use the `kernel.tar.gz` in this repository instead.

#### Software Architecture

AArch64

#### Installation

`dnf install raspberrypi-kernel`

#### Instructions

1. The version of raspberrypi kernel is in the front of `.raspi.aarch64` such as `5.10.0-4.0.0.1.raspi.aarch64`.
2. The module files will be installed into `/lib/modules/` such as `/lib/modules/5.10.0-4.0.0.1.raspi.aarch64`, where `5.10.0-4.0.0.1.raspi` is the version of raspberrypi kernel.
3. The image file will be installed into `/boot/` such as `/boot/vmlinuz-5.10.0-4.0.0.1.raspi.aarch64`. Meanwhile, the `/boot/kernel8.img` will be overrided by the image file.
4. The device tree files will be installed into `/boot/` such as `/boot/dtb-5.10.0-4.0.0.1.raspi.aarch64`. Meanwhile, the `.dtb` files and `overlays` folder in this `dtb-5.10.0-4.0.0.1.raspi.aarch64` directory will replace the corresponding files in `/boot/`.



#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
